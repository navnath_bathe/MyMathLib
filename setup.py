from setuptools import setup

setup(name='MyMathLib',
      version='0.1',
      description='One line library description',
      url='git@gitlab.com:navnath_bathe/MyMathLib.git',
      author='navnath bathe',
      author_email='n.bathe10@gmail.com',
      license='MIT',
      packages=['MyMathLib'],
      install_requires=['xxhash'],
      zip_safe=False)
